#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>

using namespace cv;

constexpr double pi{3.141592654};
constexpr int fftthres{16};

enum class dft_type
{
	forward, inverse
};

enum class degradation_type
{
	defocus, motion
};

int mydft(Mat& src, Mat& dst, const dft_type type);
int wiener_filter(Mat& src, Mat& deg, Mat& dst, const Ptr<Mat> noise=nullptr, const Ptr<Mat> orig=nullptr);
int least_square_filter(Mat& src, Mat& deg, Mat& dst, const double mean=0, const double deviation=0);
int BLPF(Mat& src, Mat& dst, const double freq, const int order);
int inverse_filter(Mat& src, Mat& deg, Mat& dst, const double radius);
int shift_mydft(Mat& src, Mat& dst);
int homework(const string& s, const degradation_type type, double lpcutoff=0, double defocus=0, double mx=0, double my=0);



int
main()
{
	std::cout<<"Please specify the radius of LP filter(|radius|<0.1 means no filter): "<<std::endl;
	double r;
	std::cin>>r;
	homework("./monarch_defocus_k0.002.bmp", degradation_type::defocus, r, 0.002);
	homework("./soccer_motion_a0.02_b0.02_T1.bmp", degradation_type::motion, r, 0, 0.02, 0.02);
	return 0;
}

int
homework(const string& s, const degradation_type type, double lpcutoff, double defocus, double motion_x, double motion_y)
{
	Mat image{imread(s, CV_LOAD_IMAGE_COLOR)};
	if (!image.data)
	{
		std::cout<<"Could not load image."<<std::endl;
		exit(-1);
	}

	int c{image.channels()};
	int m{image.rows};
	int n{image.cols};
	int d{image.depth()};


	//display some basic information
	std::cout<<s<<std::endl;
	std::cout<<"chan="<<c<<";\trows="<<m<<";\tcols="<<n<<";\tdep="<<d<<std::endl;


	//degradation function
	//deg[1] is for testing purpose
	Mat deg[]{
		{m, n, CV_64FC2, Scalar::all(0)},
		{m, n, CV_64FC2, Scalar::all(0)},
	};


	//initialize degradation function
	double x=(m-1)/2.0;
	double y=(n-1)/2.0;
	if (type==degradation_type::defocus)
	{
		for (int u=0; u<m; u++)
			for (int v=0; v<n; v++)
			{
				deg[0].at<std::complex<double>>(u, v)=\
				std::complex<double>(std::exp(-defocus*std::pow((u-x)*(u-x)+(v-y)*(v-y), 5.0/6.0)), 0);
				deg[1].at<std::complex<double>>(u, v)=\
				std::complex<double>(std::exp(defocus*std::pow((u-x)*(u-x)+(v-y)*(v-y), 5.0/6.0)), 0);
#ifdef limit
				for(int i=0;i<2;i++)
				{
					double scale=std::abs(deg[i].at<std::complex<double>>(u, v));
					if(scale<1e-2)
					deg[i].at<std::complex<double>>(u, v)*=(1e-2/scale);
				}
#endif
			}
	}
	else
	{
		double temp;
		for (int u=0; u<m; u++)
			for (int v=0; v<n; v++)
			{
				temp=pi*((u-x)*motion_x+(v-y)*motion_y);
				if (std::abs(temp)<0.001)
				{
					deg[0].at<std::complex<double>>(u, v)=std::complex<double>(1, 0);
					deg[1].at<std::complex<double>>(u, v)=std::complex<double>(1, 0);
				}
				else
				{
					double a=std::abs(std::sin(temp)/temp);
					std::complex<double> b=((a>0)?1.0:-1.0)*(std::polar(a, -temp));
					deg[0].at<std::complex<double>>(u, v)=b;
					//avoid dividing by zero
					if(abs(b)>1e-10)
						deg[1].at<std::complex<double>>(u, v)=double(1)/deg[0].at<std::complex<double>>(u, v);
					else
						deg[1].at<std::complex<double>>(u, v)=std::complex<double>(1e10,0);
				}
#ifdef limit
				for (int i=0; i<2; i++)
				{
					double scale=std::abs(deg[i].at<std::complex<double>>(u, v));
					if(scale<1e-2)
						deg[i].at<std::complex<double>>(u, v)*=(1e-2/scale);
				}
#endif
			}
	}

		
	std::vector<Mat> result(3);
	Mat*chan{new Mat[c]}, *spat{new Mat[c]}, *freq{new Mat[c]};
	Mat*filfreq_inv{new Mat[c]}, *filspat_inv{new Mat[c]};
	Mat*filfreq_wie{new Mat[c]}, *filspat_wie{new Mat[c]};
	Mat*filfreq_con{new Mat[c]}, *filspat_con{new Mat[c]};

	//split up into channels
	split(image, chan);
	for (int i=0; i<c; i++)
	{
		std::cout<<"channel"<<i<<":..."<<std::flush;

		{
			Mat planes[]={Mat_<double>(chan[i]), Mat::zeros(m, n, CV_64FC1)};
			merge(planes, 2, spat[i]);
		}

		dft(spat[i], freq[i], DFT_COMPLEX_OUTPUT);
		//mydft(spat[i], freq[i], dft_type::forward);
		shift_mydft(freq[i], freq[i]);

		//filfreq_inv[i]=freq[i];
		//filfreq_wie[i]=filfreq[i];
		inverse_filter(freq[i], deg[0], filfreq_inv[i], lpcutoff);
		wiener_filter(freq[i], deg[0], filfreq_wie[i]);
		least_square_filter(freq[i],deg[0],filfreq_con[i],0.0,std::sqrt(20.0));


		shift_mydft(filfreq_inv[i], filfreq_inv[i]);
		shift_mydft(filfreq_wie[i], filfreq_wie[i]);
		shift_mydft(filfreq_con[i],filfreq_con[i]);

		//mydft(filfreq[i], filspat[i], dft_type::inverse);
		dft(filfreq_inv[i], filspat_inv[i], DFT_INVERSE+DFT_REAL_OUTPUT);
		dft(filfreq_wie[i], filspat_wie[i], DFT_INVERSE+DFT_REAL_OUTPUT);
		dft(filfreq_con[i],filspat_con[i],DFT_INVERSE+DFT_REAL_OUTPUT);

		std::cout<<"OK."<<std::endl;
	}

	merge(filspat_inv, c, result[0]);
	merge(filspat_wie, c, result[1]);
	merge(filspat_con, c, result[2]);

	for (int i=0; i<3; i++)
	{
		normalize(result[i], result[i], 0, 255, NORM_MINMAX);
		result[i].convertTo(result[i], CV_8UC3);
	}

	string p=(type==degradation_type::defocus)?"defocus":"motion";
	imwrite(p+"_inverse.png", result[0]);
	imwrite(p+"_wiener.png", result[1]);
	imwrite(p+"_constrained_least_square.png", result[2]);

	//transform into a viewable image form (double between values 0 and 1)
	//	
	//	namedWindow("inverse filter", CV_WINDOW_AUTOSIZE);
	//	imshow("inverse filter", result);
	//	waitKey(0);

	
	delete[] chan;
	delete[] spat;
	delete[] freq;
	delete[] filfreq_inv;
	delete[] filspat_inv;
	delete[] filfreq_wie;
	delete[] filspat_wie;
	delete[] filfreq_con;
	delete[] filspat_con;
	
	return (0);
}

int
mydft(Mat& src, Mat& dst, const dft_type type)
{
	//only expects a Mat of type CV_64FC2
	//dst will be CV_64FC2 for forward transform, and CV_64FC1 for inverse transform
	//implemented with radix-2 FFT,but only a partial implementation
	if (src.type()!=CV_64FC2)
	{
		std::cout<<"mydft():src type unexpected"<<std::endl;
		exit(src.type());
	}

	int m{src.rows};
	int n{src.cols};


	static int counter=-1;
	counter++;
	if (counter==0)
	{
		//only deals with limited cases
		int m_=m, c=0;
		while (m_>fftthres&&((c=m_%2)==0))
			m_=m_/2;
		if (c!=0)
		{
			std::cout<<"dft():Cannot gurantee dft performance,abort!"<<std::endl;
			std::cout<<((type==dft_type::forward)?"forward":"inverse")<<std::endl;
			exit(m_);
		}
	}


	if (m<=fftthres)
	{

		std::vector<std::vector<std::complex<double>>> M1;
		M1.resize(m);
		for (int i=0; i<m; i++)
			M1[i].resize(m);

		std::vector<std::vector<std::complex<double>>> M2;
		M2.resize(n);
		for (int i=0; i<n; i++)
			M2[i].resize(n);


		//dft coefficients
		double c1=-1.0, c2=1.0;
		if (type==dft_type::inverse)
		{
			c1=1.0;
			c2=1.0/(m*n);
		}

		//initialize coefficients
		for (int i=0; i<m; i++)
			for (int j=0; j<i+1; j++)
				M1[i][j]=std::polar((double) 1.0, c1*2*pi/m*i*j);
		for (int i=0; i<m; i++)
			for (int j=m-1; i<j; j--)
				M1[i][j]=M1[j][i];
		for (int i=0; i<n; i++)
			for (int j=0; j<i+1; j++)
				M2[i][j]=std::polar((double) 1.0, c1*2*pi/n*i*j);
		for (int i=0; i<n; i++)
			for (int j=n-1; i<j; j--)
				M2[i][j]=M2[j][i];


		//dft	
		//copy src to support in-place operations
		Mat src_copy=src.clone();
		Mat dst_temp{m, n, CV_64FC2, Scalar::all(0)};
		for (int u=0; u<m; u++)
			for (int v=0; v<n; v++)
				for (int i=0; i<m; i++)
					for (int j=0; j<n; j++)
						dst_temp.at<std::complex<double>>(u, v)+=src_copy.at<std::complex<double>>(i, j)*M1[u][i]*M2[v][j];



		dst_temp*=c2;
		dst_temp.copyTo(dst);

	}
	else
	{
		int x=m/2;
		int y=n/2;
		Mat EE(x, y, CV_64FC2, Scalar::all(0));
		Mat EO(x, y, CV_64FC2, Scalar::all(0));
		Mat OE(x, y, CV_64FC2, Scalar::all(0));
		Mat OO(x, y, CV_64FC2, Scalar::all(0));

		//radix-2 decomposition
		for (int i=0; i<x; i++)
			for (int j=0; j<y; j++)
			{
				EE.at<std::complex<double>>(i, j)=src.at<std::complex<double>>(2*i, 2*j);
				EO.at<std::complex<double>>(i, j)=src.at<std::complex<double>>(2*i, 2*j+1);
				OE.at<std::complex<double>>(i, j)=src.at<std::complex<double>>(2*i+1, 2*j);
				OO.at<std::complex<double>>(i, j)=src.at<std::complex<double>>(2*i+1, 2*j+1);
			}


		//fft
		mydft(EE, EE, type);
		mydft(EO, EO, type);
		mydft(OE, OE, type);
		mydft(OO, OO, type);


		double c1=-1.0;
		if (type==dft_type::inverse)
			c1=1.0;

		for (int u=0; u<x; u++)
		{
			std::complex<double> temp1=std::polar((double) 1.0, c1*pi/x*u);
			for (int v=0; v<y; v++)
			{
				std::complex<double> temp2=std::polar((double) 1.0, c1*pi/y*v);
				EO.at<std::complex<double>>(u, v)*=temp2;
				OE.at<std::complex<double>>(u, v)*=temp1;
				OO.at<std::complex<double>>(u, v)*=(temp1*temp2);
			}
		}


		dst.create(m, n, CV_64FC2);
		Mat q0={dst, Rect(0, 0, y, x)};
		Mat q1={dst, Rect(y, 0, y, x)};
		Mat q2={dst, Rect(0, x, y, x)};
		Mat q3={dst, Rect(y, x, y, x)};


		q0=EE+EO+OE+OO;
		q1=EE-EO+OE-OO;
		q2=EE+EO-OE-OO;
		q3=EE-EO-OE+OO;


	}

	//real output
	if (counter==0&&type==dft_type::inverse)
	{
		Mat C[2];
		split(dst, C);
		C[0].copyTo(dst);
	}
	counter--;
	return 0;
}

int
shift_mydft(Mat& src, Mat& dst)
{
	int m{src.rows};
	int n{src.cols};
	if (src.type()!=CV_64FC2||m%2!=0||n%2!=0)
	{
		std::cout<<"shift_mydft():src type unexpected"<<std::endl;
		exit(src.type());
	}

	int y{n/2};
	int x{m/2};

	src.copyTo(dst);

	Mat q0(dst, Rect(0, 0, y, x)); // Top-Left
	Mat q1(dst, Rect(y, 0, y, x)); // Top-Right
	Mat q2(dst, Rect(0, x, y, x)); // Bottom-Left
	Mat q3(dst, Rect(y, x, y, x)); // Bottom-Right

	Mat tmp; // swap quadrants (Top-Left with Bottom-Right)
	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp); // swap quadrant (Top-Right with Bottom-Left)
	q2.copyTo(q1);
	tmp.copyTo(q2);

	return 0;
}

//K=0.005 is best for motion
//K=0.05 is best for defocus?;

int
wiener_filter(Mat& src, Mat& deg, Mat& dst, const Ptr<Mat> noise, const Ptr<Mat> orig)
{
	if (src.type()!=CV_64FC2||deg.type()!=CV_64FC2||src.rows!=deg.rows||src.cols!=deg.cols)
	{
		std::cout<<"wiener_filter():matrices type unexpected or mismatch"<<std::endl;
		exit(-1);
	}

	int m{src.rows};
	int n{src.cols};
	Mat temp{m, n, CV_64FC2, Scalar::all(0)};

	if (noise==nullptr||orig==nullptr)
	{

		double K=0;
		std::cout<<"Wiener filter:choose a value for 'K': ";
		std::cin>>K;

		for (int u=0; u<m; u++)
			for (int v=0; v<n; v++)
			{
				temp.at<std::complex<double>>(u, v)=src.at<std::complex<double>>(u, v)*\
 std::conj(deg.at<std::complex<double>>(u, v))/\
 (std::pow(std::abs(deg.at<std::complex<double>>(u, v)), 2)+K);
			}
		temp.copyTo(dst);

	}
	else
	{
		//won't implement
	}
	return 0;
}

int
least_square_filter(Mat& src, Mat& deg, Mat& dst, const double mean, const double deviation)
{
	if (src.type()!=CV_64FC2||deg.type()!=CV_64FC2||src.rows!=deg.rows||src.cols!=deg.cols)
	{
		std::cout<<"least_square_filter():matrices type unexpected or mismatch"<<std::endl;
		exit(-1);
	}

	int m{src.rows};
	int n{src.cols};
	
	//initialize laplacian matrix
	Mat lap{m, n, CV_64FC1, Scalar::all(0)};
	lap.at<double>(0,1)=lap.at<double>(1,0)=lap.at<double>(2,1)=lap.at<double>(1,2)=-1.0;
	lap.at<double>(1,1)=4.0;
	dft(lap,lap,DFT_COMPLEX_OUTPUT);
	shift_mydft(lap, lap);
	
	double noise{m*n*(mean*mean+deviation*deviation)};
	std::cout<<"noise="<<noise<<std::endl;

	Mat temp{m, n, CV_64FC2, Scalar::all(0)},res{m, n, CV_64FC2, Scalar::all(0)};
	double gamma{0},R{0};
	//gamma iteration here
	//manual iteration
	std::cout<<"initial value for gamma is "<<gamma<<std::endl;
	do
	{
		for (int u=0; u<m; u++)
			for (int v=0; v<n; v++)
			{
				temp.at<std::complex<double>>(u, v)=src.at<std::complex<double>>(u, v)*\
				std::conj(deg.at<std::complex<double>>(u, v))/\
				(std::pow(std::abs(deg.at<std::complex<double>>(u, v)), 2)+\
				gamma*std::pow(std::abs(lap.at<std::complex<double>>(u, v)), 2));
				
				res.at<std::complex<double>>(u, v)=src.at<std::complex<double>>(u, v)-deg.at<std::complex<double>>(u, v)*\
				temp.at<std::complex<double>>(u, v);
				
			}
		
		R=std::pow(norm(res),2);
		std::cout<<R<<std::endl;
		if(R<noise)
			std::cout<<"please specify a larger gamma or a negtive number to use the current value"<<std::endl;
		else
			std::cout<<"please specify a smaller gamma or a negtive number to use the current value"<<std::endl;
	
	}while((std::abs(R-noise)/noise)>0.1&&(std::cin>>gamma)&&gamma>0);
	
	//std::cout<<temp;
	temp.copyTo(dst);
	
	return 0;
}

int
BLPF(Mat& src, Mat& dst, const double freq, const int order)
{
	if (src.type()!=CV_64FC2)
	{
		std::cout<<"BLPF():src type unexpected"<<std::endl;
		exit(src.type());
	}

	int m{src.rows};
	int n{src.cols};


	std::vector<std::vector<double>> D;
	D.resize(m);
	for (int i=0; i<m; i++)
		D[i].resize(n);

	//initialize D(u,v)
	double x{(m-1)/double(2.0)};
	double y{(n-1)/double(2.0)};
	for (int i=0; i<int((m+1)/2); i++)
		for (int j=0; j<int((n+1)/2); j++)
			D[i][j]=std::sqrt((i-x)*(i-x)+(j-y)*(j-y));
	for (int i=0; i<int((m+1)/2); i++)
		for (int j=n/2; j<n; j++)
			D[i][j]=D[i][n-1-j];
	for (int i=m/2; i<m; i++)
		for (int j=0; j<int((n+1)/2); j++)
			D[i][j]=D[m-1-i][j];
	for (int i=m/2; i<m; i++)
		for (int j=n/2; j<n; j++)
			D[i][j]=D[i][n-1-j];

	double temp;
	Mat dst_temp{m, n, CV_64FC2, Scalar::all(0)};
	for (int u=0; u<m; u++)
		for (int v=0; v<m; v++)
		{
			temp=1/(1+std::pow((D[u][v])/freq, 2*order));
			dst_temp.at<std::complex<double>>(u, v)=src.at<std::complex<double>>(u, v)*temp;
		}
	dst_temp.copyTo(dst);

	return 0;
}

int
inverse_filter(Mat& src, Mat& deg, Mat& dst, const double radius)
{
	if (src.type()!=CV_64FC2||deg.type()!=CV_64FC2||src.rows!=deg.rows||src.cols!=deg.cols)
	{
		std::cout<<"inverse_filter():matrices type unexpected or mismatch"<<std::endl;
		exit(-1);
	}

	int m{src.rows};
	int n{src.cols};
	Mat temp{m, n, CV_64FC2, Scalar::all(0)};

	for (int i=0; i<m; i++)
		for (int j=0; j<n; j++)
			temp.at<std::complex<double>>(i, j)=src.at<std::complex<double>>(i, j)/deg.at<std::complex<double>>(i, j);

	temp.copyTo(dst);

	if (std::abs(radius)>0.1)
		BLPF(temp, dst, radius, 20);

	return 0;
}














